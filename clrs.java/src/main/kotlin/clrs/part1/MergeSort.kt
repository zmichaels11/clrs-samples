package clrs.part1

fun merge(arr: IntArray, l: Int, m: Int, r: Int): Int {
    val n1 = m - l + 1
    val n2 = r - m

    val L = IntArray(n1) { arr[l + it] }
    val R = IntArray(n2) { arr[m + 1 + it] }

    var i = 0
    var j = 0
    var k = l

    while (i < n1 && j < n2) {
        if (L[i] <= R[j]) {
            arr[k] = L[i]
            i++
        } else {
            arr[k] = R[j]
            j++
        }

        k++
    }

    while (i < n1) {
        arr[k] = L[i]
        i++
        k++
    }

    while (j < n2) {
        arr[k] = R[j]
        j++
        k++
    }

    return (r - l + 1)
}

fun mergeSort(arr: IntArray, l: Int, r: Int): Int {
    var merges = 0

    if (l < r) {
        val m = (l + r) / 2

        merges += mergeSort(arr, l, m)
        merges += mergeSort(arr, m + 1, r)

        merges += merge(arr, l, m, r)
    }

    return merges
}

fun mergeSort(arr: IntArray) {
    val merges = mergeSort(arr, 0, arr.size - 1)

    println("Merges: $merges")
}

private fun mergeBottomUp(arr: IntArray, tmp: IntArray, l: Int, m: Int, r: Int): Int {
    for (k in l .. r) {
        tmp[k] = arr[k]
    }

    var i = l
    var j = m + 1

    for (k in l .. r) {
        arr[k] = when {
            i > m -> tmp[j++]
            j > r -> tmp[i++]
            tmp[j] < tmp[i] -> tmp[j++]
            else -> tmp[i++]
        }
    }

    return (r - l + 1)
}

fun mergeSortBottomUp(arr: IntArray) {
    var len = 1
    val tmp = IntArray(arr.size)
    var merges = 0

    while (len < arr.size) {
        var l = 0

        while (l < arr.size - len) {
            val m = l + len - 1
            val r = Math.min(l + len + len - 1, arr.size - 1)

            merges += mergeBottomUp(arr, tmp, l, m, r)
            l += 2 * len
        }

        len *= 2
    }

    println("Total merges: $merges")
}
