package clrs.part1

fun insertionSort(arr: IntArray) {
    // iterate across the array; start on index 1 because we compare with previous
    for (j in 1 until arr.size) {
        val key = arr[j]
        var i = j - 1

        // shift array until key >= arr[i]
        while (i >= 0 && arr[i] > key) {
            arr[i + 1] = arr[i]
            i -= 1
        }

        arr[i + 1] = key
    }
}