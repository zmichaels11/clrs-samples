package ds;

import java.util.Objects;

public final class DoubleLinkedList<T> implements List<T> {
    public static final class Node<T> {
        public final T value;
        private Node<T> prev, next;

        private Node(final T value, final Node<T> prev, final Node<T> next) {
            this.value = value;
            this.prev = prev;
            this.next = next;
        }

        private Node(final T value) {
            this(value, null, null);
        }
    }

    private final Node<T> head = new Node<>(null);
    private final Node<T> tail = new Node<>(null);
    private int size = 0;

    public DoubleLinkedList() {
        this.head.next = this.tail;
        this.tail.prev = this.head;
    }

    @Override
    public void add(final T value) {
        this.addTail(value);
    }

    public void addTail(final T value) {
        final var oldTail = this.tail.prev;
        final var newTail = new Node<>(value, oldTail, this.tail);

        oldTail.next = newTail;
        this.tail.prev = newTail;
        this.size ++;
    }

    public void addHead(final T value) {
        final var oldHead = this.head.next;
        final var newHead = new Node<>(value, this.head, oldHead);

        oldHead.prev = newHead;
        this.head.next = newHead;
        this.size++;
    }

    public T getHead() {
        return this.head.next.value;
    }

    public T getTail() {
        return this.tail.prev.value;
    }

    @Override
    public boolean remove(final T value) {
        for (Node<T> cursor = this.head.next, prev = this.head; cursor != this.tail; prev = cursor, cursor = cursor.next) {
            if (Objects.equals(cursor.value, value)) {
                prev.next = cursor.next;
                cursor.next.prev = prev;
                this.size--;
                return true;
            }
        }

        return false;
    }

    @Override
    public int getSize() {
        return this.size;
    }

    @Override
    public void clear() {
        this.head.next = this.tail;
        this.tail.prev = this.head;
        this.size = 0;
    }

    @Override
    public boolean contains(final T value) {
        for (var cursor = this.head.next; cursor != this.tail; cursor = cursor.next) {
            if (Objects.equals(cursor.value, value)) {
                return true;
            }
        }

        return false;
    }
}
