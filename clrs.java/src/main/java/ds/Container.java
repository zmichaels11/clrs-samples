package ds;

public interface Container<T> {
    int getSize();

    void clear();

    boolean contains(T value);

    default boolean isEmpty() {
        return this.getSize() == 0;
    }
}
