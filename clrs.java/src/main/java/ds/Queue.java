package ds;

public interface Queue<T> extends Container<T> {
    void pushBack(T value);

    T popFront();
}
