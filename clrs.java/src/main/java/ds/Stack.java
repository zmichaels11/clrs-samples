package ds;

public interface Stack<T> extends Container<T> {
    void push(T value);

    T pop();
}
