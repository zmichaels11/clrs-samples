package ds;

import java.util.Objects;

public final class ArrayList<T> implements RandomAccessList<T>, ArrayBacked {
    private static final int INITIAL_SIZE = 8;

    private Object[] elements = new Object[INITIAL_SIZE];
    private int size;

    @Override
    public void reserve(final int size) {
        if (size > this.elements.length) {
            final var newSize = Math.max(size, this.elements.length * 2);
            final var newArray = new Object[newSize];

            System.arraycopy(this.elements, 0, newArray, 0, this.size);
            this.elements = newArray;
        }
    }

    @Override
    public void resize(final int size) {
        this.reserve(size);
        this.size = size;
    }

    @Override
    public void add(final T value) {
        this.reserve(this.size + 1);
        this.elements[this.size++] = value;
    }

    @Override
    public boolean remove(final T value) {
        for (int i = 0; i < this.size; i++) {
            if (Objects.equals(this.elements, value)) {
                final int remaining = this.size - i;

                for (int j = 0; j < remaining; j++, i++) {
                    this.elements[i] = this.elements[i + j];
                }

                return true;
            }
        }

        return false;
    }

    @Override
    public boolean contains(final T value) {
        for (int i = 0; i < this.size; i++) {
            if (Objects.equals(this.elements, value)) {
                return true;
            }
        }

        return false;
    }

    @Override
    public void clear() {
        this.size = 0;
    }

    @Override
    public int getSize() {
        return this.size;
    }

    @Override
    public T getAt(final int index) {
        return (T) this.elements[index];
    }

    @Override
    public void setAt(final int index, final T value) {
        this.elements[index] = value;
    }

    @Override
    public void trim() {
        if (this.elements.length == this.size) {
            return;
        }

        final var newArray = new Object[this.size];

        System.arraycopy(this.elements, 0, newArray, 0, this.size);

        this.elements = newArray;
    }

    @Override
    public String toString() {
        final var out = new StringBuilder();

        out.append("ArrayList: [");

        for (int i = 0; i < this.size; i++) {
            out.append(Objects.toString(this.elements[i]));

            if (i < this.size - 1) {
                out.append(", ");
            }
        }

        out.append("]");

        return out.toString();
    }
}
