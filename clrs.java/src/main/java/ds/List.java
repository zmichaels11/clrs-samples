package ds;

public interface List<T> extends Container<T> {
    void add(T value);

    boolean remove(T value);
}
