package ds;

public interface RandomAccessList<T> extends List<T>, RandomAccess<T> {
    void reserve(int size);

    void resize(int size);
}
