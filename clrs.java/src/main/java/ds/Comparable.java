package ds;

public interface Comparable<T> {
    int compareTo(T other);
}
