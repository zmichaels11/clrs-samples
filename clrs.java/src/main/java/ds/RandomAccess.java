package ds;

public interface RandomAccess<T> {
    void setAt(int index, T value);

    T getAt(int index);
}
