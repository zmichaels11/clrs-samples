package ds;

import java.util.Objects;
import java.util.Optional;

public final class LinkedList<T> implements List<T> {
    public static final class Node<T> {
        public final T value;
        private Node<T> next;

        private Node(final T value) {
            this(value, null);
        }

        private Node(final T value, final Node<T> next) {
            this.value = value;
            this.next = next;
        }

        public Optional<Node<T>> getNext() {
            return Optional.ofNullable(this.next);
        }

        @Override
        public String toString() {
            return Objects.toString(this.value);
        }
    }

    private final Node<T> head = new Node<>(null);
    private Node<T> tail = head;
    private int size = 0;

    private void reattachTail() {
        var cursor = this.head;

        while (cursor != null) {
            this.tail = cursor;
            cursor = cursor.next;
        }
    }

    @Override
    public void add(T value) {
        final var newTail = new Node<>(value);

        this.tail.next = newTail;
        this.tail = newTail;
        this.size ++;
    }

    @Override
    public boolean remove(T value) {
        var previous = this.head;
        var cursor = this.head.next;

        while (cursor != null) {
            if (Objects.equals(cursor.value, value)) {
                previous.next = cursor.next;
                this.reattachTail();
                this.size --;
                return true;
            }

            previous = cursor;
            cursor = cursor.next;
        }

        return false;
    }

    @Override
    public int getSize() {
        return this.size;
    }

    @Override
    public void clear() {
        this.head.next = null;
        this.tail = this.head;
        this.size = 0;
    }

    @Override
    public boolean contains(T value) {
        var cursor = this.head.next;

        while (cursor != null) {
            if (Objects.equals(cursor.value, value)) {
                return true;
            }

            cursor = cursor.next;
        }

        return false;
    }

    @Override
    public String toString() {
        final var out = new StringBuilder();

        out.append("LinkedList: [");

        for (var cursor = this.head.next; cursor != null; cursor = cursor.next) {
            out.append(cursor);

            if (cursor.next != null) {
                out.append(", ");
            }
        }

        out.append("]");

        return out.toString();
    }
}
