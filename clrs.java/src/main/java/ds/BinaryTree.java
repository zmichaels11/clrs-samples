package ds;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

public final class BinaryTree<T> {
    protected static final class Node<T> {
        protected final T value;
        protected final int index;

        private Node(final T value, final int index) {
            this.value = value;
            this.index = index;
        }
    }

    private final List<Node<T>> elements = new ArrayList<>();

    public BinaryTree() {
        this.elements.add(null);
    }

    public int size() {
        return this.elements.size() - 1;
    }

    public boolean isEmpty() {
        return this.size() == 0;
    }

    protected boolean hasLeft(final Node<T> node) {
        return 2 * node.index < this.size();
    }

    protected boolean hasRight(final Node<T> node) {
        return 2 * node.index + 1 < this.size();
    }

    protected Node<T> left(final Node<T> node) {
        return this.elements.get(2 * node.index);
    }

    protected Node<T> right(final Node<T> node) {
        return this.elements.get(2 * node.index + 1);
    }

    protected Node<T> parent(final Node<T> node) {
        return this.elements.get(node.index / 2);
    }

    protected T replace(final Node<T> node, final T newValue) {
        final int position = node.index;
        final var oldValue = this.elements.get(position).value;

        this.elements.set(node.index, new Node<>(newValue, position));

        return oldValue;
    }

    protected Optional<Node<T>> root() {
        if (this.isEmpty()) {
            return Optional.empty();
        } else {
            return Optional.of(this.elements.get(1));
        }
    }

    public Optional<T> getRoot() {
        if (this.isEmpty()) {
            return Optional.empty();
        } else {
            return Optional.of(this.elements.get(1).value);
        }
    }

    public void add(final T value) {
        final int position = this.size() + 1;

        this.elements.add(new Node<>(value, position));
    }

    public T remove() {
        if (this.isEmpty()) {
            throw new IndexOutOfBoundsException();
        }

        return this.elements.remove(this.size()).value;
    }

    public Iterator<T> iterator() {
        final var tmp = new ArrayList<T>(this.size());

        for (int i = 1; i < this.elements.size(); i++) {
            tmp.add(this.elements.get(i).value);
        }

        return tmp.iterator();
    }

    public Stream<T> values() {
        return this.elements.stream().skip(1).map(node -> node.value);
    }

    public void clear() {
        this.elements.clear();
        this.elements.add(null);
    }
}
