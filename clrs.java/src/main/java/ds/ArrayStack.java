package ds;

import java.util.Objects;

public final class ArrayStack<T> implements Stack<T>, RandomAccess<T>, ArrayBacked {
    private static final int INITIAL_SIZE = 8;
    private final RandomAccessList<T> backing = new ArrayList<>();
    private int cursor = 0;

    @Override
    public void push(final T value) {
        this.backing.resize(this.cursor + 1);
        this.backing.setAt(this.cursor++, value);
    }

    @Override
    public T pop() {
        return this.backing.getAt(--this.cursor);
    }

    @Override
    public int getSize() {
        return this.cursor;
    }

    @Override
    public void clear() {
        this.cursor = 0;
        this.backing.clear();
    }

    @Override
    public boolean contains(final T value) {
        for (int i = 0; i < this.cursor; i++) {
            if (Objects.equals(this.backing.getAt(i), value)) {
                return true;
            }
        }

        return false;
    }

    @Override
    public void trim() {
        if (this.backing instanceof ArrayBacked) {
            ((ArrayBacked) this.backing).trim();
        }
    }

    @Override
    public void setAt(final int index, final T value) {
        this.backing.setAt(index, value);
    }

    @Override
    public T getAt(final int index) {
        return this.backing.getAt(index);
    }

    @Override
    public String toString() {
        final var out = new StringBuilder();

        out.append("ArrayStack: [");

        for (int i = 0; i < this.cursor; i++) {
            out.append(this.backing.getAt(i));

            if (i < this.cursor - 1) {
                out.append(", ");
            }
        }

        out.append("]");

        return out.toString();
    }
}
