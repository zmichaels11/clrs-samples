package ds;

public interface Tree<T> extends Container<T> {
    interface Node<T> {
        T getValue();

        List<T> getChildren();

        T getParent();
    }

    Node<T> getRoot();

    void add(T value);
}
