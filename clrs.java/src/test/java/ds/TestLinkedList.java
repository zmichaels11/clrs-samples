package ds;

import org.junit.Assert;
import org.junit.Test;

public class TestLinkedList {
    @Test
    public void runTest() {
        final var list = new LinkedList<Integer>();

        Assert.assertTrue(list.isEmpty());

        list.add(0);

        Assert.assertFalse(list.isEmpty());
        Assert.assertTrue(list.remove(0));
        Assert.assertFalse(list.contains(0));

        for (int i = 0; i < ELEMENTS; i++) {
            list.add(i);
        }

        System.out.println(list);

        Assert.assertFalse(list.isEmpty());
        Assert.assertTrue(list.remove(0));
        Assert.assertFalse(list.remove(0));

        Assert.assertTrue(list.remove(42));
        Assert.assertFalse(list.contains(42));

        Assert.assertTrue(list.contains(1));
    }

    private static final int ELEMENTS = 1000;
}
