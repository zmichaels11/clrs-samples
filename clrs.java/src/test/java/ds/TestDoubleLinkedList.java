package ds;

import org.junit.Assert;
import org.junit.Test;

public class TestDoubleLinkedList {
    @Test
    public void runTest() {
        final var list = new DoubleLinkedList<Integer>();

        Assert.assertTrue(list.isEmpty());

        list.addTail(0);

        Assert.assertFalse(list.isEmpty());
        Assert.assertTrue(list.remove(0));
        Assert.assertFalse(list.contains(0));

        for (int i = 0; i < ELEMENTS; i++) {
            list.addTail(i);
        }

        System.out.println(list);

        Assert.assertFalse(list.isEmpty());
        Assert.assertTrue(list.remove(0));
        Assert.assertFalse(list.remove(0));

        Assert.assertTrue(list.remove(42));
        Assert.assertFalse(list.contains(42));

        Assert.assertTrue(list.contains(1));

        list.addHead(-1);
        Assert.assertEquals(-1, list.getHead().intValue());
        Assert.assertEquals(ELEMENTS - 1, list.getTail().intValue());

        System.out.println("DoubleLinkedList tests completed!");
    }

    private static final int ELEMENTS = 1000;
}
