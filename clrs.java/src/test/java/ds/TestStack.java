package ds;

import org.junit.Assert;
import org.junit.Test;

public class TestStack {
    @Test
    public void runTest() {
        final var stack = new ArrayStack<Integer>();

        Assert.assertTrue(stack.isEmpty());

        stack.push(0);
        Assert.assertTrue(stack.contains(0));
        Assert.assertEquals(0, stack.pop().intValue());
        Assert.assertTrue(stack.isEmpty());

        for (int i = 0; i < ELEMENTS; i++) {
            stack.push(i);
        }

        System.out.println(stack);

        for (int i = ELEMENTS - 1; i >= 0; i--) {
            Assert.assertEquals(i, stack.pop().intValue());
        }

        Assert.assertTrue(stack.isEmpty());

        System.out.println("Stack tests completed!");
    }

    private final int ELEMENTS = 1000;
}
