package clrs

import org.junit.Assert
import java.util.*
import kotlin.random.Random

fun sortTest(fn: (IntArray) -> Unit) {
    // best-case; values are [0, LENGTH) increasing; no relocations necessary
    val presortedArray = IntArray(LENGTH) { it }

    fn(presortedArray)
    assertSorted(presortedArray)

    // worst-case test; values are (0, LENGTH] decreasing; each element must be relocated
    val unsurtedArray = IntArray(LENGTH) { LENGTH - it }

    fn(unsurtedArray)
    assertSorted(unsurtedArray)

    val rng = Random(System.currentTimeMillis())

    // randomized input
    for (i in 0 until ITERATIONS) {
        val randomizedArray = IntArray(LENGTH) { rng.nextInt() }

        fn(randomizedArray)
        assertSorted(randomizedArray)
    }
}

private fun assertSorted(arr: IntArray) {
    if (DEBUG) {
        println("Actual: ${Arrays.toString(arr)}")
    }

    val sorted = arr.sortedArray()

    if (DEBUG) {
        println("expected: ${Arrays.toString(arr)}")
    }

    Assert.assertArrayEquals(sorted, arr)
}

var DEBUG = false
private const val LENGTH = 1000
private const val ITERATIONS = 10