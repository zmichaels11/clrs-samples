package clrs.part1

import clrs.DEBUG
import clrs.sortTest
import org.junit.Test

class TestMergeSortBottomUp {
    @Test
    fun testMergeSort() {
        DEBUG = true
        sortTest(::mergeSortBottomUp)
        System.out.println("testMergeSort completed!")
    }
}