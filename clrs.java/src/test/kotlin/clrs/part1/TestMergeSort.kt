package clrs.part1

import clrs.sortTest
import org.junit.Test

class TestMergeSort {
    @Test
    fun testMergeSort() {
        sortTest(::mergeSort)
        System.out.println("testMergeSort completed!")
    }
}