package clrs.part1

import clrs.sortTest
import org.junit.Test

class TestInsertionSort {
    @Test
    fun testInsertionSort() {
        sortTest(::insertionSort)
        System.out.println("testInsertionSort completed!")
    }
}